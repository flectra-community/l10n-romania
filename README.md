# Flectra Community / l10n-romania

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[l10n_ro_config](l10n_ro_config/) | 2.0.1.2.0| Romania - Localization Install and Config Applications
[l10n_ro_account_report_invoice](l10n_ro_account_report_invoice/) | 2.0.1.1.0| Romania - Invoice Report
[l10n_ro_payment_to_statement](l10n_ro_payment_to_statement/) | 2.0.1.0.2| Add payment to cash statement
[l10n_ro_stock](l10n_ro_stock/) | 2.0.1.0.0| Romania - Stock
[currency_rate_update_RO_BNR](currency_rate_update_RO_BNR/) | 2.0.1.2.0| Currency Rate Update National Bank of Romania service
[l10n_ro_siruta](l10n_ro_siruta/) | 2.0.1.2.0| Romania - Siruta
[l10n_ro_payment_receipt_report](l10n_ro_payment_receipt_report/) | 2.0.1.3.0| Romania - Payment Receipt Report
[l10n_ro_account_edit_currency_rate](l10n_ro_account_edit_currency_rate/) | 2.0.1.2.0| Romania - Invoice Edit Currency Rate
[l10n_ro_nondeductible_vat](l10n_ro_nondeductible_vat/) | 2.0.1.2.0| Romania - Nondeductible VAT
[l10n_ro_partner_create_by_vat](l10n_ro_partner_create_by_vat/) | 2.0.2.0.0| Romania - Partner Create by VAT
[l10n_ro_address_extended](l10n_ro_address_extended/) | 2.0.1.1.0| Romania - Extended Addresses
[l10n_ro_account_period_close](l10n_ro_account_period_close/) | 2.0.2.0.0| Romania - Account Period Closing
[l10n_ro_fiscal_validation](l10n_ro_fiscal_validation/) | 2.0.1.3.0| Romania - Fiscal Validation
[l10n_ro_partner_unique](l10n_ro_partner_unique/) | 2.0.1.1.0| Creates a rule for vat and nrc unique for partners.
[l10n_ro_stock_report](l10n_ro_stock_report/) | 2.0.3.3.0| Romania - Stock Report
[l10n_ro_city](l10n_ro_city/) | 2.0.2.1.0| Romania - City
[l10n_ro_vat_on_payment](l10n_ro_vat_on_payment/) | 2.0.1.2.0| Romania - VAT on Payment
[l10n_ro_stock_account](l10n_ro_stock_account/) | 2.0.2.7.0| Romania - Stock Accounting


