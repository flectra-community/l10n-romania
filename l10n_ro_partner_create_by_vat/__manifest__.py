# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    "name": "Romania - Partner Create by VAT",
    "category": "Localization",
    "summary": "Romania - Partner Create by VAT",
    "depends": ["l10n_ro_config"],
    "license": "AGPL-3",
    "version": "2.0.2.0.0",
    "author": "NextERP Romania,"
    "Forest and Biomass Romania,"
    "Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/l10n-romania",
    "installable": True,
    "development_status": "Mature",
    "data": ["views/partner_view.xml"],
    "maintainers": ["feketemihai"],
}
